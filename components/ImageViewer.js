import { Image, StyleSheet } from 'react-native'

export function ImageViewer({ placeholderImageSource, image }) {
    return (
        <Image source={image ? { uri: image } : placeholderImageSource} style={styles.image} />
    )
}

const styles = StyleSheet.create({
    image: {
        width: 320,
        height: 440,
        borderRadius: 18,
    },
})
